const express = require('express')
const bodyParser = require('body-parser')
const sequelize = require('sequelize')
const app = express()
const db = require('./models')
const userRoutes = require('./routers/users')
const memberRoutes = require('./routers/members')
const bookingRoutes = require('./routers/bookings')
const facilityRoutes = require('./routers/facilities')
//synchronizing the database and forcing it to false so we dont lose data
db.sequelize.sync({ force: false }).then(() => {
    console.log("db has been re sync")
})

const port = 3000

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' })
})

app.use(userRoutes)
app.use(memberRoutes)
app.use(bookingRoutes)
app.use(facilityRoutes)


app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})