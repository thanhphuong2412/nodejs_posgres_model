//importing modules

const db = require("../models");
//Assigning db.users to User variable
const Facilitie = db.facilities;


const getFacilities = async (req, res, next) => {
    try {

        const facilities = await Facilitie.findAll({})

        res.status(200).json(facilities)

    } catch (error) {
        console.log(error);
    }
};


const createFacilitie = async (req, res, next) => {
    try {
        console.log("heheh");
        const data = req.body;
        // console.log(data)
        const newFacilitie = await Facilitie.create(data);

        return res.status(200).json(newFacilitie);
    } catch (error) {
        console.log(error);
        return res.status(400).json({ error });
    }
};
module.exports = { createFacilitie, getFacilities};
