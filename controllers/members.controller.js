//importing modules
const { members } = require("../models");
const db = require("../models");
//Assigning db.users to User variable
const Member = db.members;

const createMember = async (req, res, next) => {
    try {
        console.log("heheh")
        const data = req.body
        // console.log(data)
        const newMember = await Member.create(data)

        return res.status(200).json(newMember)

    } catch (error) {
        console.log(error);
        return res.status(400).json({error})
    }
};
module.exports = { createMember };
