//importing modules
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const db = require("../models");
//Assigning db.users to User variable
const Booking = db.bookings;
const Member = db.members;

const populate = async (req, res, next) => {
    try {
        const getNew = await Booking.findAll({
            subQuery: false,
            include: [
                {
                    model: Member,
                    // as: "mbs",
                    attributes: ['firstname', 'surname'],
                    required: true,
                    right: true, // has no effect, will create an inner join,
                    where: {
                        surname: {
                            [Op.like]: "%Farrell%",
                        },
                    },
                },
            ],
        });
        console.log(getNew.length)
        return res.status(200).json(getNew)
    } catch (error) {
        console.log(error)
        console.log("jasdjahsdjhasjdha");
    }
};
module.exports = { populate };
