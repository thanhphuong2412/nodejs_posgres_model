//importing modules
const db = require("../models");
//Assigning db.users to User variable
const User = db.users;

const createUser = async (req, res, next) => {
    try {
        const data = req.body

        const user = await User.create(data)

        return res.status(200).json(user)

    } catch (error) {
        console.log(error);
    }
};


const getUser = async (req, res, next) => {
    try {

        const users = await User.findAll({})

        res.status(200).json({ users })

    } catch (error) {
        console.log(error);
    }
};

const getUserById = async (req, res, next) => {
    try {
        const { id } = req.params

        const user = await User.findOne({
            where: {
                id
            },
        });

        if (!user) return res.status(500).json({ message: 'cannot get user' })

        return res.status(200).json({ user })

    } catch (error) {
        console.log(error);
    }
};

const updateUserById = async (req, res, next) => {
    try {
        const { id } = req.params
        const data = req.body

        const user = await User.findOne({
            where: {
                id
            },
        });

        if (!user) res.status(500).json({ message: 'cannot get user' })

        await user.set(data).save()

        res.status(200).json({ user })

    } catch (error) {
        console.log(error);
    }
};

const deleteUserById = async (req, res, next) => {
    try {
        const { id } = req.params
        const data = req.body

        const user = await User.findOne({
            where: {
                id
            },
        });

        if (!user) res.status(500).json({ message: 'cannot get user' })

        await user.destroy()

        res.status(200).json({ user })

    } catch (error) {
        console.log(error);
    }
};


//exporting module
module.exports = {
    createUser, getUser, getUserById, updateUserById, deleteUserById
};