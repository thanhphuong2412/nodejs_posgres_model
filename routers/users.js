//importing modules
const express = require('express')
const userController = require('../controllers/users.controller')

const router = express.Router()

//passing the middleware function to the signup
router.post('/users', userController.createUser)
router.get('/users', userController.getUser)
router.get('/users/:id', userController.getUserById)

router.get('/users/:id', userController.getUserById)
router.put('/users/:id', userController.updateUserById)


router.delete('/users/:id', userController.deleteUserById)


module.exports = router