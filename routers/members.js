//importing modules
const express = require('express')
const memberController  = require('../controllers/members.controller')

const router = express.Router()

router.use('/members',memberController.createMember)

module.exports = router