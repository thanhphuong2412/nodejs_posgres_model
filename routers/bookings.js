//importing modules
const express = require('express')
const controller  = require('../controllers/bookings.controller')

const router = express.Router()

router.use('/bookings',controller.populate)

module.exports = router