//importing modules
const express = require('express')
const controller  = require('../controllers/facilities.controller')

const router = express.Router()

router.use('/facilities',controller.getFacilities)
// router.use('/populate',controller.populate)

module.exports = router