//importing modules
const {Sequelize, DataTypes} = require('sequelize')
//Database connection with dialect of postgres specifying the database we are using
//port for my database is 5433
//database name is discover
const sequelize = new Sequelize(`postgres://postgres:123456@localhost:5432/askany`, {dialect: "postgres"})

//checking if connection is done
    sequelize.authenticate().then(() => {
        console.log(`Database connected to discover`)
    }).catch((err) => {
        console.log(err)
    })

    const db = {}
    db.Sequelize = Sequelize
    db.sequelize = sequelize

//connecting to model
db.users = require('./users.model') (sequelize, DataTypes)
db.members = require('./members.model') (sequelize, DataTypes)
db.bookings = require('./bookings.model') (sequelize, DataTypes)
db.facilities = require('./facilities.model') (sequelize, DataTypes)

//foreignKey
// db.members.hasMany(db.bookings,{ as:'bookings'})
// db.bookings.belongsToMany(db.members,{
//     foreignKey:'memberid',
//     as: "booking",
// })

Object.keys(db).forEach((modelName) => {
    if('associate' in db[modelName]){
        console.log("hohiho")
        db[modelName].associate(db)
    }
    // console.log({db})
})


//exporting the module
module.exports = db