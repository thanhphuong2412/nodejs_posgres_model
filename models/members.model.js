module.exports = (sequelize, DataTypes) => {
    const Member = sequelize.define(
        "members",
        {
            surname: {
                type: DataTypes.STRING(200),
                allowNull: false,
            },
            firstname: {
                type: DataTypes.STRING(200),
                allowNull: false,
            },
            address: {
                type: DataTypes.STRING(300),
            },
            zipcode: {
                type: DataTypes.STRING,
            },
            telephone: {
                type: DataTypes.STRING,
            },
            recommendedby: {
                type: DataTypes.STRING,
            },
            joindate: {
                type: DataTypes.DATE,
            },
            memid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            }
        },
        { timestamps: true },
        // {
        //     associate: (models) => {
        //         Member.belongsTo(models.Booking, {
        //             foreignKey: { name: "memid" },
        //         });
        //     },
        // }
        
    );
    Member.associate = function (models) {
        console.log("memberber");
        Member.hasMany(models.bookings, {
            // through: "BookingHasMoreMember",
            foreignKey: "memid",
            as:'booking',

        });
        models.bookings.belongsTo(Member,{
            foreignKey: "memid",
            // as:'Member',
        });
    };
    return Member;
};
