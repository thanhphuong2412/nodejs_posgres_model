module.exports = (sequelize, DataTypes) => {
    const Facilitie = sequelize.define(
        "facilities",
        {
            facid: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
            },
            name: {
                type: DataTypes.STRING(100),
                allowNull: false,
            },
            membercost: {
                type: DataTypes.DOUBLE,
                allowNull: false,
            },
            initialoutlay: {
                type: DataTypes.INTEGER,
            },
            monthlymaintenance: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            guestcost: {
                type: DataTypes.DOUBLE,
                allowNull: false,
            },
        },
        { timestamps: true }
    );
    Facilitie.associate = function (models) {
        Facilitie.hasMany(models.bookings, {
            foreignKey: "facid",
        });

    //     models.Booking.belongsTo(Facilitie);
    };
    return Facilitie;
};

// Facilitie.hasMany(Bookings, {
//     foreignKey: "facid",
// });

// Bookings.belongsTo(Facilitie);
