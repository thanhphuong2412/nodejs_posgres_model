//user model
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("users", {
        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        // createdAt:{
        //     type:  DataTypes.DATE,
        //     defaultValue: DataTypes.NOW 
        // },
        // updatedAt:{
        //     type:  DataTypes.DATE,
        //     defaultValue: DataTypes.NOW 
        // },
  
    }, { timestamps: true },)
    return User
}