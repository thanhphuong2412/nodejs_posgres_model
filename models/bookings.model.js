const Members = require("./members.model");
const Facilities = require("./facilities.model");
module.exports = (sequelize, DataTypes) => {
    const Booking = sequelize.define(
        "bookings",
        {
            // memid: {
            //     type: DataTypes.INTEGER,
            //     allowNull: false,
            // },
            bookid: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            slots: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            starttime: {
                type: DataTypes.DATE,
            },
            // facid: {
            //     type: DataTypes.INTEGER,
            //     //    defaultValue: DataTypes.UUIDV4,
            // },
        },
        { timestamps: true },
        // {
        //     associate: (models) => {
        //         Booking.hasMany(models.Member, {
        //             through: "BookingHasMoreMember",
        //             foreignKey:'memid',d
        //         });
        //     },
        // }
    );

    // Booking.associate = function (models) {
    //     console.log("Booking Booking");
    //     console.log(models.facilities);
    //     Booking.belongsTo(models.Members, {
    //        foreignKey: "memid",

    //     });
    //     // models.members.belongsToMany(Booking);
    // };
    return Booking;
};
